terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.28.0"
    }
  }
}

provider "aws" {
  profile    = "default"
  region     = "eu-north-1"
  access_key = "XXX"
  secret_key = "YYY"
}







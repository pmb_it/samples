resource "aws_ec2_managed_prefix_list" "home" {
  name           = "home"
  address_family = "IPv4"
  max_entries    = 5

  entry {
    cidr        = "10.0.0.1/32"
    description = "home"
  }


  tags = {
    Env = "live"
  }
}
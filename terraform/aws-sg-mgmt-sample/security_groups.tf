
resource "aws_security_group" "terra_sg_management" {
  name        = "terra_sg_management"
  description = "terra_sg_management"

  ingress {
    description     = "HTTP"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    prefix_list_ids = [aws_ec2_managed_prefix_list.home.id]
  }


  ingress {
    description     = "HTTPS"
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    prefix_list_ids = [aws_ec2_managed_prefix_list.ext_routers.id, aws_ec2_managed_prefix_list.home.id]
  }

  ingress {
    description     = "SSH"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    prefix_list_ids = [aws_ec2_managed_prefix_list.ext_routers.id, aws_ec2_managed_prefix_list.home.id,aws_ec2_managed_prefix_list.aws_jenkins.id]
  }
  ingress {
    description     = "Zabbix Agent"
    from_port       = 10050
    to_port         = 10050
    protocol        = "tcp"
    prefix_list_ids = [aws_ec2_managed_prefix_list.ext_routers.id, aws_ec2_managed_prefix_list.home.id]
  }

  ingress {
    description     = "Zabbix Proxy"
    from_port       = 10051
    to_port         = 10051
    protocol        = "tcp"
    prefix_list_ids = [aws_ec2_managed_prefix_list.ext_routers.id, aws_ec2_managed_prefix_list.home.id]
  }

  ingress {
    description     = "icmp"
    protocol        = "icmp"
    from_port       = 8
    to_port         = 0
    prefix_list_ids = [aws_ec2_managed_prefix_list.external_pollers.id]
  }

 egress {
    description     = "ANY TO WORLD"
    protocol        = "-1"
    from_port       = 0
    to_port         = 0
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "terra_sg_management"
  }

}
resource "aws_instance" "myinstance" {
  ami                     = "ami-78628505"
  instance_type           = "t2.micro"
  disable_api_termination = true
  vpc_security_group_ids  = [aws_security_group.terra_sg_management.id,aws_security_group.terra_sg_common_http.id]
  tags = {
    Name   = "myinstance"
    backup = "yes"
  }
}


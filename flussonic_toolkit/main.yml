---
- name: ansible-sat-flussonic
  hosts: all
  become: true
  gather_facts: true
# -------------------------------------------------------------------------------------
  tasks:
# -------------------------------------------------------------------------------------
  - name: vars_host management actions
    block:
    - name: generate deploy id for backup
      shell: "date +'%Y%m%d%H%M%S'"
      register: deploy_id

    - name: print deploy id
      debug:
        msg: "Deploy id: {{ deploy_id.stdout }}"
  
    - name: create tmp dir if not exists
      file:
        path: "{{ tmp_dir }}"
        owner: root
        group: root
        mode: '0751'
        state: directory

    - name: cleanup tmp dir if exists
      file:
        state: "{{ item }}"
        path: "{{ tmp_dir }}"
      with_items:
        - absent
        - directory
      become: true

    - name: get aws access key
      shell: cat /home/user/.aws/credentials  | grep aws_access_key_id | awk '{print $3}'
      register: aws_access_key
      become: true

    - name: get aws secret key
      shell: cat /home/user/.aws/credentials | grep aws_secret_access_key | awk '{print $3}'
      register: aws_secret_key
      become: true

    - name: get flussonic secrets from aws secret manager
      set_fact:
        "{{ item }}": "{{ (lookup('aws_secret', 'prod/flussonic/credentials', region='eu-north-1', aws_access_key=aws_access_key.stdout, aws_secret_key=aws_secret_key.stdout) | from_json).get(item) }}"
      loop:
        - fls_cluster_key
        - fls_view_auth_login
        - fls_view_auth_pass
        - fls_edit_auth_login
        - fls_edit_auth_pass
        - fls_securetoken
        - fls_license_key

    - name: get google secrets from aws secret manager
      set_fact:
        "{{ item }}": "{{ (lookup('aws_secret', 'prod/google/credentials', region='eu-west-1', aws_access_key=aws_access_key.stdout, aws_secret_key=aws_secret_key.stdout) | from_json).get(item) }}"
      loop:
        - ggl_project_id
        - ggl_private_key_id
        - ggl_private_key
        - ggl_client_email
        - ggl_client_id
        - ggl_client_x509_cert_url

    - name: template service_account.json
      template:
        src: templates/service_account.json.j2
        dest: "{{ strm_google_sa }}"
        owner: root
        group: root
        mode: '0751'
      become: true

    - name: install python gsheets library via pip (local)
      pip: 
        name: google-sheets-to-csv 
        executable: pip3
      become: true

    - name: export stream csv from gdrive
      shell: "gs-to-csv --service-account-credential-file {{ strm_google_sa }} -f {{ strm_google_ss }} {{ strm_google_ws }} {{ strm_tmp_dir }}"

    - name: read streams from CSV file and return a dictionary
      read_csv:
        path: "{{ strm_csv_file }}"
        key: alias
        delimiter: ","
        strict: true
      register: streams
    # rem csv file

    - name: remove google_sa.json (credentials)
      file: 
        path: "{{ strm_google_sa }}"
        state: absent

    when: inventory_hostname == vars_host
# -------------------------------------------------------------------------------------
  - name: config generation delegated actions
    block:
    - name: set tmp config parts path for each server
      set_fact:
        tmp_config_dir: "{{ tmp_dir }}/{{ inventory_hostname }}"

    - name: set final config paths for each server
      set_fact:
        fls_fin_conf: "{{ tmp_dir }}/{{ inventory_hostname }}.conf"

    - name: create tmp config dirs for each server
      file:
        path: "{{ tmp_config_dir }}"
        owner: root
        group: root
        mode: '0751'
        state: directory

    - name: create base config 
      template:
        src: "templates/base.conf.j2"
        dest: "{{ tmp_config_dir }}/base.conf"
        owner: root
        group: root
        mode: '0700'

    - name: create dvr config include 
      template:
        src: "templates/streams/dvr.conf.j2"
        dest: "{{ tmp_config_dir }}/streams_dvr.conf"
        owner: root
        group: root
        mode: '0700'
      when: "'dvr' in group_names"

    - name: create origin config include
      template:
        src: "templates/streams/origin.conf.j2"
        dest: "{{ tmp_config_dir }}/streams_origin.conf"
        owner: root
        group: root
        mode: '0700'
      when: group_names is search("origin")
              
    - name: create trans config include 
      template:
        src: "templates/streams/trans.conf.j2"
        dest: "{{ tmp_config_dir }}/streams_trans.conf"
        owner: root
        group: root
        mode: '0700'
      when: group_names is search("trans")
      
    - name: create sources config include 
      template:
        src: "templates/relations/sources.conf.j2" #
        dest: "{{ tmp_config_dir }}/sources.conf" #
        owner: root
        group: root
        mode: '0700'
      when: sources is defined
      
    - name: create peers config include 
      template:
        src: "templates/relations/peers.conf.j2" #
        dest: "{{ tmp_config_dir }}/peers.conf" #
        owner: root
        group: root
        mode: '0700'
      when: peers is defined

    - name: create lbs config include 
      template:
        src: "templates/relations/lbs.conf.j2" #
        dest: "{{ tmp_config_dir }}/lbs.conf" #
        owner: root
        group: root
        mode: '0700'
      when: lbs is defined
               
    - name: compile and validate flussonic.conf for each server # refactoring, hosts limit
      shell: "cd {{ tmp_config_dir }} && sudo cat * > {{ fls_cfg_path }} && /opt/flussonic/bin/validate_config -j && cp {{ fls_cfg_path }} {{ fls_fin_conf }} && chown {{ deploy_user }}:{{ deploy_user}} {{ fls_fin_conf }}" 
      become: true

    delegate_to: localhost
    when: group_names is search('flussonic')
# -------------------------------------------------------------------------------------
  - name: flussonic server deploy actions
    block:  
      - name: create backup dir if does not exist
        file:
          path: "{{ fls_cfg_bkp_dir }}"
          owner: root
          group: root
          mode: '0751'
          state: directory
        become: true

      - name: backup flussonic config before deployment
        copy: 
          src: "{{ fls_cfg_path }}"
          dest: "{{ fls_cfg_bkp_dir }}/{{ hostvars[vars_host]['deploy_id'].stdout }}.conf"
          remote_src: true
        become: true

      - name: deploy license
        template:
          src: templates/license.txt.j2
          dest: /tmp/license.txt
          owner: root
          group: root
          mode: '0700'
        become: true
        
      - name: deploy updated flussonic config
        copy: 
          src: "{{ fls_fin_conf }}"
          dest: "/tmp/flussonic.conf"
          owner: root
          group: root
          mode: '0700'
        become: true

    when: group_names is search('flussonic') 




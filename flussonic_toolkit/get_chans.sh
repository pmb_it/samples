#!/bin/bash
for STREAM in `cat /etc/flussonic/flussonic.conf | grep "stream" | awk '{print $2}' | grep -vi ingest`
do
        INPUT=`grep  "stream $STREAM {" /etc/flussonic/flussonic.conf -A 5 | grep input | awk '{print $2,$3}' | sed -e "s/\;//"`
        CAPTURE=`grep "stream $STREAM {" /etc/flussonic/flussonic.conf -A 5 | grep capture_at | awk '{print $2}' | grep -P "origin|trans-3\-(\d+)"`
        echo "$STREAM|$INPUT|$CAPTURE"
done
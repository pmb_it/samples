#!/bin/bash
NAME="$1"
case "$NAME" in
        *edge-1) NUM="2";;
        *edge-2) NUM="1";;
esac;
PEER_NAME=$(echo "$NAME" | sed -s "s/edge-[1-2]/edge-$NUM/")
nslookup "$PEER_NAME" | grep -i name -A 1 | tail -n 1 | awk '{print $2}'


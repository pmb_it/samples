#!/bin/bash
MODE=$1
MODULE=$2
GROUP=$3


if [[ -z "$GROUP" || -z "$MODE" || -z "$MODULE" ]]
then
	echo "Usage: $0 [deploy|reconfig|delete|test] [customapp2|customapp1|all] [locs list|all]"
    echo -en "Example 1: $0 deploy customapp2 loc1,loc2\nExample 2: $0 reconfig customapp2 all\n"
	exit 1
fi

LOG="logger -s -t edgetool"

case "$MODULE" in
    customapp2*|customapp1*)
    ;;
    all)
        MODULE="customapp2,customapp1"
    ;;
    *)
        echo "Incorrect module selected, try \"customapp2|customapp1\""
        exit 1
    ;;
esac
case "$MODE" in
    deploy|reconfig|delete)
        git config --global user.name "$SUDO_USER" | $LOG
        git add . 2>&1 | $LOG
        git commit -am "MODE=$MODE, MODULE=$MODULE, GROUP=$GROUP" 2>&1 | $LOG
        git push origin master 2>&1 | $LOG

        if [[ "$MODE" == "deploy" ]]
            then MODE="deploy,reconfig,test"
        fi

        ansible-playbook -i hosts.ini --limit "$GROUP" --tags "$MODE,$MODULE" edgetool.yml 2>&1 | $LOG
    ;;
    test)
        ansible-playbook -i hosts.ini --limit "$GROUP" --tags "$MODE,$MODULE" edgetool.yml  2>&1 | $LOG
    ;;
    *)
        echo "Try deploy|reconfig|delete|test"
    exit 1
    ;;
esac
